import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите размер массива");
        int size = input.nextInt();
        int array[] = new int[size];
        System.out.println("Введите элементы массива");
        for (int i = 0; i < size; i++) {
            array[i] = input.nextInt();
        }
        System.out.println("Введите искомое число");
        int searchedNumber = input.nextInt();
        int searchedNumberIndex = searchNumberInArray(array, searchedNumber);
        System.out.println("Индекс искомого числа");
        System.out.println(searchedNumberIndex);
        sortToLeftZeroNumbers(array);
    }
   //Функция для поиска индекса искомого числа в массиве
    static int searchNumberInArray(int array[], int searchedNumber) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == searchedNumber) {
                return i;
            }
        }
        return -1;
    }
    //Процедура, которая переместит все значимые элементы влево, заполнив нулевые
    static void sortToLeftZeroNumbers(int array[]) {
        int[] sortArray = array;
        int swapCount = 0;
        int lastIndex = sortArray.length-1;

        for(int i = lastIndex-1; i >=0; i--) {
            if(sortArray[i] == 0) {
                sortArray[i] = sortArray[lastIndex-swapCount];
                sortArray[lastIndex-swapCount] = 0;
                swapCount++;
            }
        }

        System.out.println("Сортированный массив");
        for(int element : sortArray) {
            System.out.print(element + ", ");
        }
    }
}