import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        System.out.println("Введите элементы массива");
        boolean arrayEnd = false;
        int i = 0;
        while (arrayEnd == false) {
            int currentNumber = input.nextInt();
            if (currentNumber == -1) {
                arrayEnd = true;
            }
            else {
                if (currentNumber >= -100 && currentNumber <= 100) {
                    arrayList.add(i, currentNumber);
                    i++;
                }
            }
        }
       System.out.println(getLeastOccurringNumber(arrayList));
    }
    //Функция возвращающая число встречающиеся минимальное количество раз
    static int getLeastOccurringNumber(ArrayList<Integer> arrayList) {
        int currentNumber = 0, leastOccurringTimes = 0, leastOccurringNumber = 0, countNumber = 0;
        for (int i = 0; i<arrayList.size(); i++) {
            currentNumber = arrayList.get(i);
            for (int j = 0; j<arrayList.size(); j++) {
                if (currentNumber == arrayList.get(j)) {
                    countNumber++;
               }
            }
           if (i == 0) {
               leastOccurringTimes = countNumber;
           }
           if (Integer.MAX_VALUE > countNumber && leastOccurringTimes > countNumber) {
                leastOccurringTimes = countNumber;
                leastOccurringNumber = arrayList.get(i);
           }
            countNumber = 0;
        }
        return leastOccurringNumber;
    }
}
