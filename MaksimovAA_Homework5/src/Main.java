import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int flag = -1;
        int currentNumber = 0;
        int i = 0;
        int min = 0;

        while (currentNumber != flag){
            currentNumber = input.nextInt();
            if (i == 0) {
                min = currentNumber;
            }
            if (currentNumber < min && currentNumber != -1) {
               min = currentNumber;
            }
            i++;
        }
        System.out.println("Ответ: " + min);
    }
}
